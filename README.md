﻿# DrawView

#### 项目介绍
- 项目名称：DrawView
- 所属系列：openharmony的第三方组件适配移植
- 功能：ohos视图，允许用户创建绘图。从简单视图中绘制任何你喜欢的图案。
- 项目移植状态：主功能完成（模拟器暂不支持运行此项目，请用真机运行）
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：master分支
#### 效果演示
画笔绘制
 
 <img src="img/1.gif"></img>
 
放大缩小画布
  
 <img src="img/2.gif"></img>
#### 安装教程
            1.在项目根目录下的build.gradle文件中，
             allprojects {
                repositories {
                    maven {
                              url 'https://s01.oss.sonatype.org/content/repositories/releases/'
                          }
                }
             }
            2.在entry模块的build.gradle文件中，
             dependencies {
                implementation('com.gitee.chinasoft_ohos:DrawView:1.0.0')
        

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
 将DrawView添加到布局中
```java
<com.byox.drawview.views.DrawView
        ohos:id="$+id:draw_view"
        ohos:width="match_parent"
        ohos:height="match_parent"
        ohos:bottom_margin="80vp"
        ohos:top_margin="60vp"
        app:dv_draw_alpha="255"
        app:dv_draw_anti_alias="true"
        app:dv_draw_color="$color:thumColor"
        app:dv_draw_corners="round"
        app:dv_draw_dither="true"
        app:dv_draw_enable_zoom="true"
        app:dv_draw_font_family="default_font"
        app:dv_draw_font_size="12"
        app:dv_draw_max_zoom_factor="8"
        app:dv_draw_mode="0"
        app:dv_draw_style="2"
        app:dv_draw_tool="0"
        app:dv_draw_width="4"
        app:dv_draw_zoomregion_maxscale="5"
        app:dv_draw_zoomregion_minscale="2"
        app:dv_draw_zoomregion_scale="4"
        app:dv_draw_is_camera="true"/>
```
在Java代码中, 可以设置/更改一些DrawView的属性.
```java
mDrawView.setDrawColor(newPaint.getColor().getValue())  //设置画布颜色
        .setPaintStyle(newPaint.getStyle())             //定义视图的绘制样式
        .setDrawWidth((int) newPaint.getStrokeWidth())  //设置画布宽度
        .setDrawAlpha(newPaint.getAlpha())              //绘制颜色的透明度
        .setAntiAlias(newPaint.isAntiAlias())           //使用抗锯齿值进行绘制
        .setLineCap(newPaint.getStrokeCap())            //设置画笔类型
        .setFontFamily(newPaint.getFont())              //用于绘制文本的字体系列
        .setFontSize(newPaint.getTextSize());           //设置字体大小
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

<pre>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
</pre>

