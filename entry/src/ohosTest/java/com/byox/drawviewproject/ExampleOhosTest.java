/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    /**
     * 初始化component
     */
    @Test
    public void initComponent() {

        try {
            Class mainAbilitySlice = Class.forName("com.xiaopo.flying.photolayout.slice.MainAbilitySlice");
            Method logDebug = mainAbilitySlice.getMethod("initComponent");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            logDebug.invoke(obj);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * showDrawModePopu
     */
    @Test
    public void showDrawModePopu() {
        try {
            Class mainAbilitySlice = Class.forName("com.xiaopo.flying.photolayout.slice.MainAbilitySlice");
            Method logDebug = mainAbilitySlice.getMethod("showDrawModePopu");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            logDebug.invoke(obj);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * showDrawTollPopu
     */
    @Test
    public void showDrawTollPopu() {
        try {
            Class mainAbilitySlice = Class.forName("com.xiaopo.flying.photolayout.slice.MainAbilitySlice");
            Method logDebug = mainAbilitySlice.getMethod("showDrawTollPopu");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            logDebug.invoke(obj);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * verifyPermission
     */
    @Test
    public void verifyPermission() {
        try {
            Class mainAbilitySlice = Class.forName("com.xiaopo.flying.photolayout.slice.MainAbilitySlice");
            Method logDebug = mainAbilitySlice.getMethod("verifyPermission");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            logDebug.invoke(obj);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * handlerFabClearDraw
     */
    @Test
    public void handlerFabClearDraw() {
        try {
            Class mainAbilitySlice = Class.forName("com.xiaopo.flying.photolayout.slice.MainAbilitySlice");
            Method logDebug = mainAbilitySlice.getMethod("handlerFabClearDraw");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            logDebug.invoke(obj);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}