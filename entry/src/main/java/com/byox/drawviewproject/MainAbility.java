package com.byox.drawviewproject;

import com.byox.drawviewproject.slice.MainAbilitySlice;
import com.byox.drawviewproject.util.PopuValue;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.window.service.WindowManager;
import ohos.bundle.IBundleManager;

public class MainAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        //getWindow().setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
    }

    @Override
    public void onRequestPermissionsFromUserResult (int requestCode, String[] permissions, int[] grantResults) {
        System.out.println("requestCode---------------"  +requestCode);
        switch (requestCode) {
            case PopuValue.MY_PERMISSIONS_REQUEST_CAMERA:
                // 匹配requestPermissions的requestCode
                if (grantResults.length > 0
                    && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getAbilityPackageContext().getBundleName())
                        .withAbilityName(PhotoImageAbility.class.getName())
                        .build();
                    intent.setOperation(operation);
                    startAbility(intent);
                } else {
                    // 权限被拒绝
                }
                break;
            case PopuValue.PERMISSIONS_REQUEST_CAMERA:
                System.out.println("PERMISSIONS_REQUEST_CAMERA");
                if (grantResults.length > 0) {
                    Intent cameraintent = new Intent();
                    Operation cameraoperation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getAbilityPackageContext().getBundleName())
                        .withAbilityName(MyCameraAbility.class.getName())
                        .build();
                    cameraintent.setOperation(cameraoperation);
                    startAbility(cameraintent);
                } else {
                    // 权限被拒绝
                }
                break;
        }
    }
}
