package com.byox.drawviewproject.dialogs;

import com.byox.drawviewproject.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

public class RequestTextDialog extends PopupDialog implements Component.ClickedListener {
    private Component component;
    private TextField textInput;
    private OnRequestTextListener onRequestTextListener;

    /**
     * RequestTextDialog构造方法
     *
     * @param context 上下文
     * @param contentComponent 组件
     * @param width 宽度
     * @param height 高度
     */
    public RequestTextDialog(Context context, Component contentComponent, int width, int height) {
        super(context, contentComponent);
        this.component = contentComponent;
        this.setAutoClosable(true);
        this.setCornerRadius(20);
        this.setBackColor(Color.WHITE);
        this.setAlignment(LayoutAlignment.CENTER);
        this.setSize(width, height);
        this.setCustomComponent(contentComponent);
        initView();
    }

    private void initView() {
        textInput = (TextField) component.findComponentById(ResourceTable.Id_textInput);
        textInput.setAdjustInputPanel(true);
        Text cancel = (Text) component.findComponentById(ResourceTable.Id_cancel);
        cancel.setClickedListener(this);
        Text confirm = (Text) component.findComponentById(ResourceTable.Id_confirm);
        confirm.setClickedListener(this);
    }

    public void setOnRequestTextListener(OnRequestTextListener onRequestTextListener) {
        this.onRequestTextListener = onRequestTextListener;
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_cancel:
                if (onRequestTextListener != null) {
                    onRequestTextListener.onRequestTextCancelled();
                }
                destroy();
                break;
            case ResourceTable.Id_confirm:
                if (onRequestTextListener != null) {
                    onRequestTextListener.onRequestTextConfirmed(textInput.getText());
                }
                this.destroy();
                break;
        }
    }

    public interface OnRequestTextListener {
        /**
         * onRequestTextConfirmed方法
         *
         * @param requestedText 文本
         */
        void onRequestTextConfirmed(String requestedText);

        /**
         * onRequestTextCancelled方法
         */
        void onRequestTextCancelled();
    }
}
