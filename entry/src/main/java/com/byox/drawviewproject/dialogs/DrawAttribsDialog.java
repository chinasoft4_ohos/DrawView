package com.byox.drawviewproject.dialogs;

import com.byox.drawviewproject.ResourceTable;
import com.byox.drawviewproject.component.HmCheckbox;
import com.byox.drawviewproject.component.HmRadioButton;
import com.byox.drawviewproject.component.HmSlider;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

public class DrawAttribsDialog extends PopupDialog implements Slider.ValueChangedListener, Component.ClickedListener{
    private Context mContext;
    private Paint mPaint;
    private Component component;
    private Text tv_current_red;
    private Text tv_current_green;
    private Text tv_current_blue;
    private Component preview_color;
    private Slider rslider;
    private Slider gslider;
    private Slider bslider;
    private Slider strokeWidthSlider;
    private Slider opacitySlider;
    private Slider fontSizeSlider;
    private Text textViewStrokeWidth;
    private Text textViewOpacity;
    private Text textViewFontSize;
    private Text cancel;
    private Text ok;
    private HmCheckbox antiAliasCheckbox;
    private HmCheckbox ditherCheckbox;
    private HmRadioButton rbfillRadioButton;
    private HmRadioButton rbfillStrokeRadioButton;
    private HmRadioButton rbStrokeRadioButton;
    private HmRadioButton rbButtRadioButton;
    private HmRadioButton rbRoundRadioButton;
    private HmRadioButton rbSquareRadioButton;
    private HmRadioButton rbDefaultRadioButton;
    private HmRadioButton rbMonospaceRadioButton;
    private HmRadioButton rbSansSerifRadioButton;
    private HmRadioButton rbSerifRadioButton;
    private OnCustomViewDialogListener onCustomViewDialogListener;
    private DirectionalLayout layout;

    /**
     * DrawAttribsDialog构造方法
     *
     * @param context 上下文
     * @param contentComponent 组件
     */
    public DrawAttribsDialog(Context context, Component contentComponent) {
        super(context, contentComponent);
        this.mContext = context;
        initParma();
        createDialog();
        this.setCustomComponent(component);
    }

    private void initParma() {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(mContext);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        this.setSize(displayAttributes.width - 100, displayAttributes.height -200);
        this.setAlignment(LayoutAlignment.CENTER);
        this.setCornerRadius(10);
    }

    private void createDialog() {
        component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_draw_attribs,
                null, false);
        rslider = (HmSlider) component.findComponentById(ResourceTable.Id_acsb_red);
        rslider.setValueChangedListener(this);
        gslider = (HmSlider) component.findComponentById(ResourceTable.Id_acsb_green);
        gslider.setValueChangedListener(this);
        bslider = (HmSlider) component.findComponentById(ResourceTable.Id_acsb_blue);
        bslider.setValueChangedListener(this);
        strokeWidthSlider = (HmSlider) component.findComponentById(ResourceTable.Id_acsb_stroke_width);
        strokeWidthSlider.setValueChangedListener(this);
        opacitySlider = (HmSlider) component.findComponentById(ResourceTable.Id_acsb_opacity);
        opacitySlider.setValueChangedListener(this);
        fontSizeSlider = (HmSlider) component.findComponentById(ResourceTable.Id_acsb_font_size);
        fontSizeSlider.setValueChangedListener(this);
        tv_current_red = (Text) component.findComponentById(ResourceTable.Id_tv_current_red);
        tv_current_green = (Text) component.findComponentById(ResourceTable.Id_tv_current_green);
        tv_current_blue = (Text) component.findComponentById(ResourceTable.Id_tv_current_blue);
        preview_color = component.findComponentById(ResourceTable.Id_preview_color);
        textViewStrokeWidth = (Text) component.findComponentById(ResourceTable.Id_tv_stroke_width);
        textViewOpacity = (Text) component.findComponentById(ResourceTable.Id_tv_opacity);
        textViewFontSize = (Text) component.findComponentById(ResourceTable.Id_tv_font_size);
        cancel = (Text) component.findComponentById(ResourceTable.Id_cancel);
        cancel.setClickedListener(this);
        ok = (Text) component.findComponentById(ResourceTable.Id_ok);
        ok.setClickedListener(this);
        antiAliasCheckbox = (HmCheckbox) component.findComponentById(ResourceTable.Id_chb_anti_alias);
        ditherCheckbox = (HmCheckbox) component.findComponentById(ResourceTable.Id_chb_dither);
        rbfillRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_fill);
        rbfillStrokeRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_fill_stroke);
        rbStrokeRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_stroke);
        rbButtRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_butt);
        rbRoundRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_round);
        rbSquareRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_square);
        rbDefaultRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_default);
        rbMonospaceRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_monospace);
        rbSansSerifRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_sans_serif);
        rbSerifRadioButton = (HmRadioButton) component.findComponentById(ResourceTable.Id_rb_serif);
    }

    private void initPaint() {
        strokeWidthSlider.setProgressValue((int) mPaint.getStrokeWidth());
        fontSizeSlider.setProgressValue(mPaint.getTextSize());
        antiAliasCheckbox.setChecked(mPaint.isAntiAlias());
        antiAliasCheckbox.setOnCheckedChanged(new HmCheckbox.OnCheckedChanged() {
            @Override
            public void onCheckedChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setAntiAlias(isBoolean);
                }
            }
        });
        ditherCheckbox.setChecked(mPaint.getDither());
        ditherCheckbox.setOnCheckedChanged(new HmCheckbox.OnCheckedChanged() {
            @Override
            public void onCheckedChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setDither(isBoolean);
                }
            }
        });
        rbfillRadioButton.setChecked(mPaint.getStyle() == Paint.Style.FILL_STYLE);
        rbfillRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setStyle(Paint.Style.FILL_STYLE);
                }
            }
        });
        rbfillStrokeRadioButton.setChecked(mPaint.getStyle() == Paint.Style.FILLANDSTROKE_STYLE);
        rbfillStrokeRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
                }
            }
        });
        rbStrokeRadioButton.setChecked(mPaint.getStyle() == Paint.Style.STROKE_STYLE);
        rbStrokeRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setStyle(Paint.Style.STROKE_STYLE);
                }
            }
        });
        rbButtRadioButton.setChecked(mPaint.getStrokeCap() == Paint.StrokeCap.BUTT_CAP);
        rbButtRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setStrokeCap(Paint.StrokeCap.BUTT_CAP);
                }
            }
        });
        rbRoundRadioButton.setChecked(mPaint.getStrokeCap() == Paint.StrokeCap.ROUND_CAP);
        rbRoundRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
                }
            }
        });
        rbSquareRadioButton.setChecked(mPaint.getStrokeCap() == Paint.StrokeCap.SQUARE_CAP);
        rbSquareRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
                }
            }
        });
        rbDefaultRadioButton.setChecked(mPaint.getFont() == Font.DEFAULT);
        rbDefaultRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setFont(Font.DEFAULT);
                }
            }
        });
        rbMonospaceRadioButton.setChecked(mPaint.getFont() == Font.MONOSPACE);
        rbMonospaceRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setFont(Font.MONOSPACE);
                }
            }
        });
        rbSansSerifRadioButton.setChecked(mPaint.getFont() == Font.SANS_SERIF);
        rbSansSerifRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setFont(Font.SANS_SERIF);
                }
            }
        });
        rbSerifRadioButton.setChecked(mPaint.getFont() == Font.SERIF);
        rbSerifRadioButton.setOnRadioButtonChanged(new HmRadioButton.OnRadioButtonChanged() {
            @Override
            public void onRadioButtonChanged(boolean isBoolean) {
                if (isBoolean) {
                    mPaint.setFont(Font.SERIF);
                }
            }
        });
    }

    @Override
    public void onProgressUpdated(Slider slider, int silderProgress, boolean b) {
        switch (slider.getId()) {
            case ResourceTable.Id_acsb_red:
                tv_current_red.setText(String.valueOf(silderProgress));
                int intRgbColor = Color.rgb(rslider.getProgress(), gslider.getProgress(), bslider.getProgress());
                Color color = new Color(intRgbColor);
                mPaint.setColor(color);
                priviewSetColor(color);
                break;
            case ResourceTable.Id_acsb_green:
                tv_current_green.setText(String.valueOf(silderProgress));
                int intRgbColorOne = Color.rgb(rslider.getProgress(), gslider.getProgress(), bslider.getProgress());
                Color colorOne = new Color(intRgbColorOne);
                mPaint.setColor(colorOne);
                priviewSetColor(colorOne);
                break;
            case ResourceTable.Id_acsb_blue:
                tv_current_blue.setText(String.valueOf(silderProgress));
                int intRgbColorTwo = Color.rgb(rslider.getProgress(), gslider.getProgress(), bslider.getProgress());
                Color colorTwo = new Color(intRgbColorTwo);
                mPaint.setColor(colorTwo);
                priviewSetColor(colorTwo);
                break;
            case ResourceTable.Id_acsb_stroke_width:
                mPaint.setStrokeWidth(silderProgress);
                textViewStrokeWidth.setText(mContext.getString(ResourceTable.String_stroke_width, silderProgress));
                break;
            case ResourceTable.Id_acsb_opacity:
                System.out.println("====alpha1" + opacitySlider.getProgress());
                mPaint.setAlpha(opacitySlider.getProgress() / 255.0f);
                textViewOpacity.setText(mContext.getString(ResourceTable.String_opacity, opacitySlider.getProgress()));
                break;
            case ResourceTable.Id_acsb_font_size:
                mPaint.setTextSize(silderProgress);
                textViewFontSize.setText(mContext.getString(ResourceTable.String_font_size, silderProgress));
                break;
        }
    }

    @Override
    public void onTouchStart(Slider slider) {

    }

    @Override
    public void onTouchEnd(Slider slider) {

    }

    /**
     * setPaint设置画笔
     *
     * @param paint 画笔
     */
    public void setPaint(Paint paint) {
        this.mPaint = paint;
        textViewStrokeWidth.setText(mContext.getString(ResourceTable.String_stroke_width, (int) mPaint.getStrokeWidth()));
        textViewOpacity.setText(mContext.getString(ResourceTable.String_opacity, (int) (255 * mPaint.getAlpha())));
        textViewFontSize.setText(mContext.getString(ResourceTable.String_font_size, 0));
        opacitySlider.setProgressValue((int) (255 * mPaint.getAlpha()));
        RgbColor rgbColor = RgbColor.fromArgbInt(paint.getColor().getValue());
        rslider.setProgressValue(rgbColor.getRed());
        bslider.setProgressValue(rgbColor.getBlue());
        gslider.setProgressValue(rgbColor.getGreen());
        initPaint();
    }

    private void priviewSetColor(Color color) {
        ShapeElement shapeElement = new ShapeElement();
        RgbColor rgbColor = RgbColor.fromArgbInt(color.getValue());
        shapeElement.setRgbColor(rgbColor);
        preview_color.setBackground(shapeElement);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_cancel:
                this.destroy();
                break;
            case ResourceTable.Id_ok:
                mPaint.setAlpha(opacitySlider.getProgress() / 255.0f);
                this.onCustomViewDialogListener.onRefreshPaint(mPaint);
                destroy();
                break;
        }
    }

    public void setOnCustomViewDialogListener(OnCustomViewDialogListener onCustomViewDialogListener) {
        this.onCustomViewDialogListener = onCustomViewDialogListener;
    }

    public interface OnCustomViewDialogListener {
        /**
         * onRefreshPaint刷新画笔
         *
         * @param newPaint 画笔
         */
        void onRefreshPaint(Paint newPaint);
    }
}
