package com.byox.drawviewproject.dialogs;

import com.byox.drawview.utils.PixelMapFactoryUtil;
import com.byox.drawviewproject.ResourceTable;
import com.byox.drawviewproject.util.FileUtils;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.media.image.PixelMap;

public class SaveBitmapDialog extends PopupDialog implements Component.ClickedListener{
    private Context mContext;
    private Component component;
    private Image ivCapturePreview;
    private Image ivCaptureBackview;
    private PixelMap mPreviewBitmap;
    private String mPreviewFormat;
    private OnSaveBitmapListener onSaveBitmapListener;
    private TextField textInput;
    private PixelMap cameraPixMap;
    private boolean mIsShow = false;
    private EventHandler mEventHandler;
    private Runnable task;

    /**
     * SaveBitmapDialog构造方法
     *
     * @param context 上下文
     * @param contentComponent 组件
     * @param width 宽度
     * @param height 高度
     */
    public SaveBitmapDialog(Context context, Component contentComponent, int width, int height) {
        super(context, contentComponent, width, height);
        this.component = contentComponent;
        this.mContext = context;
        this.setBackColor(Color.TRANSPARENT);
        this.setTransparent(true);
        this.setAlignment(LayoutAlignment.HORIZONTAL_CENTER);
        this.setCustomComponent(contentComponent);
        initView();
    }

    private void initView() {
        ivCapturePreview = (Image) component.findComponentById(ResourceTable.Id_iv_capture_preview);
        ivCaptureBackview = (Image) component.findComponentById(ResourceTable.Id_iv_capture_backview);
        Text cancel = (Text) component.findComponentById(ResourceTable.Id_cancel);
        cancel.setClickedListener(this);
        Text confirm = (Text) component.findComponentById(ResourceTable.Id_confirm);
        confirm.setClickedListener(this);
        textInput = (TextField) component.findComponentById(ResourceTable.Id_textInput);
        textInput.setAdjustInputPanel(true);
       /* task = new Runnable() {
            @Override
            public void run() {
                // 待执行的操作，由开发者定义
                System.out.println("onToggleSoftKeyboard==========" + mIsShow);
            }
        };*/
       // setKeyBoardListner();
    }
/*
    private void setKeyBoardListner() {
        KeyboardUtils.addKeyboardToggleListener(component, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                System.out.println("onToggleSoftKeyboard==========" + isVisible);
                if (isVisible) {
                    mIsShow = true;
                } else {
                    mIsShow = false;
                }
                if (mIsShow) {
                    //component.setMarginBottom(0);
                } else {
                    //component.setMarginTop(-300);
                }
            }
        });
        mEventHandler = new EventHandler(EventRunner.getMainEventRunner());
        mEventHandler.postTask(task, 500, EventHandler.Priority.IMMEDIATE);
    }*/

    /**
     * newInstance方法
     *
     * @param context 上下文
     * @param contentComponent 组件
     * @param width 宽度
     * @param height 高度
     * @return dialog对象
     */
    public static SaveBitmapDialog newInstance(Context context, Component contentComponent, int width, int height) {
        return new SaveBitmapDialog(context, contentComponent, width, height);
    }

    /**
     * setPreviewBitmap方法
     *
     * @param bitmap 参数
     */
    public void setPreviewBitmap(PixelMap bitmap) {
        this.mPreviewBitmap = bitmap;
        if (mPreviewBitmap != null) {
            ivCapturePreview.setPixelMap(bitmap);
        }
    }

    /**
     * setCameraPixMap方法
     *
     * @param pixMap 参数
     */
    public void setCameraPixMap(PixelMap pixMap) {
       this.cameraPixMap = pixMap;
       if (cameraPixMap != null) {
           ivCaptureBackview.setPixelMap(pixMap);
       }
    }

    /**
     * setPreviewFormat方法
     *
     * @param previewFormat 参数
     */
    public void setPreviewFormat(String previewFormat) {
        this.mPreviewFormat = previewFormat;
        final String[] fileName = {"DrawViewCapture." + mPreviewFormat.toLowerCase()};
        textInput.setText(fileName[0]);
    }

    /**
     * setOnSaveBitmapListener监听方法
     *
     * @param onSaveBitmapListener 保存bitmap监听
     */
    public void setOnSaveBitmapListener(OnSaveBitmapListener onSaveBitmapListener) {
        this.onSaveBitmapListener = onSaveBitmapListener;
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_cancel:
                if (onSaveBitmapListener != null) {
                    onSaveBitmapListener.onSaveBitmapCanceled();
                }
                break;
            case ResourceTable.Id_confirm:
                saveImage();
                break;
        }
        SaveBitmapDialog.this.destroy();
    }

    private void saveImage() {
        if (cameraPixMap == null) {
            PixelMap bitmap = PixelMapFactoryUtil.createBitmap(mPreviewBitmap);
            FileUtils.saveImageToGallery(bitmap, mContext, textInput.getText());
            if (onSaveBitmapListener != null) {
                onSaveBitmapListener.onSaveBitmapCompleted();
            }
        }else {
            PixelMap savePixmap = PixelMapFactoryUtil.getSavePixelMap(cameraPixMap, mPreviewBitmap,
                cameraPixMap.getImageInfo().size.width / 2 - mPreviewBitmap.getImageInfo().size.width / 2,
                cameraPixMap.getImageInfo().size.height / 2 - mPreviewBitmap.getImageInfo().size.height/ 2);
            FileUtils.saveImageToGallery(savePixmap, mContext, textInput.getText());
            if (onSaveBitmapListener != null) {
                onSaveBitmapListener.onSaveBitmapCompleted();
            }
        }
    }

    public interface OnSaveBitmapListener {
        /**
         * onSaveBitmapCompleted方法
         */
        void onSaveBitmapCompleted();

        /**
         * onSaveBitmapCanceled方法
         */
        void onSaveBitmapCanceled();
    }
}
