package com.byox.drawviewproject.slice;

import com.byox.drawviewproject.MainAbility;
import com.byox.drawviewproject.ResourceTable;
import com.byox.drawviewproject.SplashScreenAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenAbilitySlice extends AbilitySlice {
    private Image mImageViewLogo;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_splash_screen);
        getWindow().setStatusBarColor(Color.BLACK.getValue());
        mImageViewLogo = (Image) findComponentById(ResourceTable.Id_iv_logo);
        mImageViewLogo.setScaleX(0);
        mImageViewLogo.setScaleY(0);

    }


    // METHODS
    private void initSplashScreen() {
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setDuration(500);
        animatorValue.setDelay(200);
        animatorValue.setCurveType(Animator.CurveType.ANTICIPATE_OVERSHOOT);
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                mImageViewLogo.setScaleX(value);
                mImageViewLogo.setScaleY(value);
            }
        });
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName(getAbilityPackageContext().getBundleName())
                            .withAbilityName(MainAbility.class.getName())
                            .build();
                        intent.setOperation(operation);
                        startAbility(intent);
                        SplashScreenAbilitySlice.this.terminateAbility();
                    }
                };
                new Timer().schedule(timerTask, 1000);
            }
            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorValue.start();
    }

    @Override
    public void onActive() {
        super.onActive();
        initSplashScreen();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
