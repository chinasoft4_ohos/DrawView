package com.byox.drawviewproject;

import com.byox.drawviewproject.slice.CameraAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MyCameraAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CameraAbilitySlice.class.getName());
    }
}
