/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject.util;

import com.byox.drawviewproject.moudle.PopuContentInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * CreatPopuData
 *
 * @since 2021-08-06
 */
public class CreatPopuData {
    private static final int NUM_ONE = -1;

    private CreatPopuData() {
    }

    /** initPopuData
     *
     * @param images 图片
     * @param texts 文字
     * @param clicked 是否点击
     * @return data 数据
     */
    public static List<PopuContentInfo> initPopuData(int[] images, String[] texts, int[] clicked) {
        List data = new ArrayList<>();
        if (images == null && texts == null) {
            return Collections.emptyList();
        } else if (texts != null) {
            PopuContentInfo popuContentInfo;
            for (int i1 = 0; i1 < texts.length; i1++) {
                popuContentInfo = new PopuContentInfo();
                if (clicked != null && clicked.length == texts.length) {
                    popuContentInfo.setChecked(clicked[i1]);
                }
                if (images == null) {
                    popuContentInfo.setPopuImageResouceId(NUM_ONE);
                } else {
                    popuContentInfo.setPopuImageResouceId(images[i1]);
                }
                popuContentInfo.setPopuText(texts[i1]);
                data.add(popuContentInfo);
            }
            return data;
        }
        return Collections.emptyList();
    }
}
