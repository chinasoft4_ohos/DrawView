/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject.util;

/**
 * PopuValue
 *
 * @since 2021-08-06
 */
public class PopuValue {
    /**
     * 静态变量 CLEARHSITORY
     */
    public static final int CLEARHSITORY = 0;
    /**
     * 静态变量 int DRAWPOWER
     */
    public static final int DRAWPOWER = 1;
    /**
     * 静态变量 DRAWATTRIBUTES
     */
    public static final int DRAWATTRIBUTES = 0;
    /**
     * 静态变量 BACKGROUNDIMAGE
     */
    public static final int BACKGROUNDIMAGE = 1;
    /**
     * 静态变量 DRAWTOOL
     */
    public static final int DRAWTOOL = 2;
    /**
     * 静态变量 DRAWMODE
     */
    public static final int DRAWMODE = 3;
    /**
     * 静态变量 SAVEDRAW
     */
    public static final int SAVEDRAW = 4;
    /**
     * 静态变量 CAMERAOPTION
     */
    public static final int CAMERAOPTION = 5;
    /**
     * 静态变量 UNDO
     */
    public static final int UNDO = 0;
    /**
     * 静态变量 REDO
     */
    public static final int REDO = 1;
    /**
     * 静态变量 MY_PERMISSIONS_REQUEST_CAMERA
     */
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 1001;
    /**
     * 静态变量 PEN
     */
    public static final int PEN = 0;
    /**
     * 静态变量 LINE
     */
    public static final int LINE = 1;
    /**
     * 静态变量 ARROW
     */
    public static final int ARROW = 2;
    /**
     * 静态变量 RECTANGLE
     */
    public static final int RECTANGLE = 3;
    /**
     * 静态变量 CIRCLE
     */
    public static final int CIRCLE = 4;
    /**
     * 静态变量 CLLIPSE
     */
    public static final int CLLIPSE = 5;
    /**
     * 静态变量 READ_MEDIA
     */
    public static final String READ_MEDIA = "ohos.permission.READ_MEDIA";
    /**
     * 静态变量 CAMERA
     */
    public static final String CAMERA = "ohos.permission.CAMERA";
    /**
     * 静态变量 PERMISSIONS_REQUEST_CAMERA
     */
    public static final int PERMISSIONS_REQUEST_CAMERA = 1002;

    private PopuValue() {
    }
}
