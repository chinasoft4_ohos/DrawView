/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject.provider;

import com.byox.drawviewproject.ResourceTable;
import com.byox.drawviewproject.moudle.ImageInfo;
import com.byox.drawviewproject.moudle.ImageUri;
import com.byox.drawviewproject.util.SimpleEventBus;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.utils.net.Uri;

import java.util.List;

/**
 * PhotoListProvider
 *
 * @since 2021-08-06
 */
public class PhotoListProvider extends BaseItemProvider implements Component.ClickedListener {
    private static final int NUM_TWO = 2;
    private Context mContext;
    private List<List<ImageInfo>> pixelMaps;

    /**
     * PhotoListProvider构造方法
     *
     * @param mcontext 上下文
     * @param pixelMaps 容器
     */
    public PhotoListProvider(Context mcontext, List<List<ImageInfo>> pixelMaps) {
        this.mContext = mcontext;
        this.pixelMaps = pixelMaps;
    }

    @Override
    public int getCount() {
        return pixelMaps.size();
    }

    @Override
    public Object getItem(int i) {
        return pixelMaps.get(i);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public Component getComponent(int index, Component convertComponent, ComponentContainer componentContainer) {
        convertComponent = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_img, null, false);
        Image imageOne = (Image) convertComponent.findComponentById(ResourceTable.Id_image1);
        Image imageTwo = (Image) convertComponent.findComponentById(ResourceTable.Id_image2);
        Image imageThree = (Image) convertComponent.findComponentById(ResourceTable.Id_image3);
        System.out.println("dispalypic============" + index + "===0===" + pixelMaps.get(index).size());
        if (pixelMaps.get(index).size() > 0) {
            imageOne.setPixelMap(pixelMaps.get(index).get(0).getPixelMap());
            imageOne.setTag(pixelMaps.get(index).get(0).getUri());
            imageOne.setClickedListener(this::onClick);
        }
        if (pixelMaps.get(index).size() > 1) {
            imageTwo.setPixelMap(pixelMaps.get(index).get(1).getPixelMap());
            imageTwo.setTag(pixelMaps.get(index).get(1).getUri());
            imageTwo.setClickedListener(this::onClick);
        }
        if (pixelMaps.get(index).size() > NUM_TWO) {
            imageThree.setPixelMap(pixelMaps.get(index).get(NUM_TWO).getPixelMap());
            imageThree.setTag(pixelMaps.get(index).get(NUM_TWO).getUri());
            imageThree.setClickedListener(this::onClick);
        }
        return convertComponent;
    }

    @Override
    public void onClick(Component component) {
        Uri uri = (Uri) component.getTag();
        SimpleEventBus.getSimpleEventBus().post(new ImageUri(uri));
        mContext.terminateAbility();
    }
}