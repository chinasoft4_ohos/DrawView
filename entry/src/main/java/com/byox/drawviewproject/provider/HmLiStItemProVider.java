/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject.provider;

import com.byox.drawviewproject.ResourceTable;
import com.byox.drawviewproject.moudle.PopuContentInfo;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

/**
 * HmLiStItemProVider
 *
 * @since 2021-08-06
 */
public class HmLiStItemProVider extends BaseItemProvider {
    private static final int NUM_ONE = -1;
    private Context mContext;
    private List<PopuContentInfo> datas;

    /**
     * HmLiStItemProVider 构造方法
     *
     * @param context 上下文
     * @param datas 返回数据
     */
    public HmLiStItemProVider(Context context, List<PopuContentInfo> datas) {
        this.mContext = context;
        this.datas = datas;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int index) {
        return datas.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
        Component cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_popu_list_item,
                null, true);
        Image popuItemImage = (Image) cpt.findComponentById(ResourceTable.Id_popu_item_image);
        Text popuItemText = (Text) cpt.findComponentById(ResourceTable.Id_popu_item_text);
        ShapeElement shapeElement = new ShapeElement();
        if (datas.get(i).getChecked() == 0) {
            shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
            cpt.setBackground(shapeElement);
            popuItemText.setTextColor(Color.BLACK);
        } else {
            shapeElement.setRgbColor(RgbColor.fromArgbInt(
                    Color.getIntColor(mContext.getString(ResourceTable.String_darkgray))));
            cpt.setBackground(shapeElement);
            popuItemText.setTextColor(Color.GRAY);
        }
        int imageSourId = datas.get(i).getPopuImageResouceId();
        if (imageSourId == NUM_ONE) {
            popuItemImage.setVisibility(Component.HIDE);
        } else {
            popuItemImage.setPixelMap(imageSourId);
        }
        popuItemText.setText(datas.get(i).getPopuText());
        return cpt;
    }
}
