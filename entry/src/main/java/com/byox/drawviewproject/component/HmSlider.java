/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject.component;

import com.byox.drawviewproject.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Slider;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * HmSlider 类
 *
 * @since 2021-08-07
 */
public class HmSlider extends Slider {
    /**
     * 静态变量NUM_ONE
     */
    private static final int NUM_ONE = 100;

    /**
     * HmSlider 构造方法
     *
     * @param context 上下文
     */
    public HmSlider(Context context) {
        super(context);
    }

    /**
     * HmSlider构造方法
     *
     * @param context 上下文
     * @param attrSet 属性参数
     * @param styleName 样式名
     */
    public HmSlider(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * HmSlider 构造方法
     *
     * @param context 上下文
     * @param attrSet 属性
     */
    public HmSlider(Context context, AttrSet attrSet) {
        super(context, attrSet);
        setThumColor();
    }

    private void setThumColor() {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(mContext.getString(ResourceTable.Color_thumColor))));
        element.setCornerRadius(NUM_ONE);
        this.setThumbElement(element);
    }
}
