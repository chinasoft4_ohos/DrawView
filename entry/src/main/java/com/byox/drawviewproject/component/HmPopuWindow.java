/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject.component;

import com.byox.drawviewproject.ResourceTable;
import com.byox.drawviewproject.moudle.PopuContentInfo;
import com.byox.drawviewproject.provider.HmLiStItemProVider;
import com.byox.drawviewproject.util.PopuValue;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

import java.util.List;

/**
 * HmPopuWindow 类
 *
 * @since 2021-08-07
 */
public class HmPopuWindow extends PopupDialog {
    private static final int CORNERRADIUS = 20;
    private static final float PIVOT = 2f;
    private static final int DURATION = 300;
    /**
     * listContainer
     */
    private ListContainer listContainer;
    /**
     * liStItemProVider
     */
    private HmLiStItemProVider liStItemProVider;
    private Context mContext;
    private List<PopuContentInfo> datas;
    private Component component;

    /**
     * HmPopuWindow构造函数
     *
     * @param context 上下文
     * @param contentComponent 组件
     * @param width 宽度
     * @param height 高度
     * @param datas 容器数据
     */
    public HmPopuWindow(Context context, Component contentComponent,
                        int width, int height, List<PopuContentInfo> datas) {
        super(context, contentComponent, width, height);
        this.mContext = context;
        this.datas = datas;
        this.component = contentComponent;
        this.setAutoClosable(true);
        this.setCornerRadius(CORNERRADIUS);
        this.setBackColor(Color.TRANSPARENT);
        this.setTransparent(true);
        this.setAlignment(LayoutAlignment.RIGHT | LayoutAlignment.TOP);
        initView();
        this.setSize(width, height);
        this.setCustomComponent(component);
    }

    public ListContainer getListContainer() {
        return listContainer;
    }

    public void setListContainer(ListContainer listContainer) {
        this.listContainer = listContainer;
    }

    /**
     * customShow 方法
     *
     * @param tag 显示类型
     */
    public void customShow(int tag) {
        show();
        AnimatorProperty ap = new AnimatorProperty();
        switch (tag) {
            case PopuValue.BACKGROUNDIMAGE:
                component.setPivot(0, 0);
                break;
            case PopuValue.DRAWTOOL:
                component.setPivot(component.getWidth() / PIVOT, 0);
                break;
            default:
                component.setPivot(component.getWidth(), 0);
        }
        ap.setTarget(component)
                .scaleXFrom(0f).scaleYFrom(0f)
                .scaleX(1f).scaleY(1f)
                .setDuration(DURATION).start();
    }

    private Component initView() {
        listContainer = (ListContainer) component.findComponentById(ResourceTable.Id_listcontainer);
        liStItemProVider = new HmLiStItemProVider(mContext, datas);
        listContainer.setItemProvider(liStItemProVider);
        setListContainer(listContainer);
        return component;
    }
}
