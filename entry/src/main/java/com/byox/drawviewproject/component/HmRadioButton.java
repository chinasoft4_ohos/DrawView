/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject.component;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Component;
import ohos.agp.components.RadioButton;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

/**
 * HmRadioButton 类
 *
 * @since 2021-08-07
 */
public class HmRadioButton extends RadioButton implements Component.DrawTask, AbsButton.CheckedStateChangedListener {
    private static final int RADIUS = 30;
    private static final int STROKEWIDTH = 5;
    private static final float NUMTWO = 2f;
    private static final int TEXTSIZE = 35;
    private static final int DRAWTEXT = 10;
    private static final int FIFTEEN = 15;
    private String text;
    private String radioColor;
    private OnRadioButtonChanged mRadioButtonChanged;

    /**
     * 构造函数 HmRadioButton
     *
     * @param context 上下文
     */
    public HmRadioButton(Context context) {
        super(context);
    }

    /**
     * HmRadioButton构造方法
     *
     * @param context 上下文
     * @param attrSet 属性
     */
    public HmRadioButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        text = attrSet.getAttr("radiotext").get().getStringValue();
        radioColor = attrSet.getAttr("radioColor").get().getStringValue();
        invalidate();
    }

    /**
     * HmRadioButton构造方法
     *
     * @param context 上下文
     * @param attrSet 属性
     * @param styleName 样式名称
     */
    public HmRadioButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void invalidate() {
        super.invalidate();
        this.addDrawTask(this::onDraw);
        this.setCheckedStateChangedListener(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        ShapeElement var3 = new ShapeElement();
        var3.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        this.setButtonElement(var3);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(STROKEWIDTH);
        paint.setColor(new Color(Color.getIntColor(radioColor)));
        canvas.drawCircle(getWidth() / NUMTWO, getHeight() / NUMTWO, RADIUS, paint);
        Paint textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(TEXTSIZE);
        canvas.drawText(textPaint, text,component.getWidth() / NUMTWO + RADIUS + DRAWTEXT,
                component.getHeight() / NUMTWO + RADIUS - FIFTEEN);
        if (isChecked()) {
            Paint inPaint = new Paint();
            inPaint.setStyle(Paint.Style.FILL_STYLE);
            inPaint.setColor(new Color(Color.getIntColor(radioColor)));
            canvas.drawCircle(getWidth() / NUMTWO, getHeight() / NUMTWO, RADIUS / NUMTWO, inPaint);
        }
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean isBoolean) {
        this.setChecked(isBoolean);
        invalidate();
        if (this.mRadioButtonChanged != null) {
            this.mRadioButtonChanged.onRadioButtonChanged(isBoolean);
        }
    }

    /**
     * setOnRadioButtonChanged方法
     *
     * @param onCheckedChanged 是否选中
     */
    public void setOnRadioButtonChanged(OnRadioButtonChanged onCheckedChanged) {
        this.mRadioButtonChanged = onCheckedChanged;
    }

    /**
     * OnRadioButtonChanged 接口
     *
     * @since 2021-08-07
     */
    public interface OnRadioButtonChanged {
        /**
         * onRadioButtonChanged接口
         *
         * @param isBoolean 是否选中
         */
        void onRadioButtonChanged(boolean isBoolean);
    }
}
