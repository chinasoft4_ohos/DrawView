/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawviewproject.component;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AbsButton;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.math.BigDecimal;

/**
 * 自定义checkbox
 *
 * @since 2021-06-07
 */
public class HmCheckbox extends Checkbox implements Component.DrawTask, Checkbox.CheckedStateChangedListener {
    private static final int DIVIDE_EQUALLY = 10;
    private static final int HARF_VALUE = 2;
    private int width;
    private int height;
    private String boxColor;
    private OnCheckedChanged mOnCheckedChanged;

    /**
     * 构造函数
     *
     * @param context Context对象
     */
    public HmCheckbox(Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context context对象
     * @param attrSet 属性值
     */
    public HmCheckbox(Context context, AttrSet attrSet) {
        super(context, attrSet);
        boxColor = attrSet.getAttr("boxColor").get().getStringValue();
        invalidate();
    }

    /**
     * 构造函数
     *
     * @param context context对象
     * @param attrSet 属性值
     * @param styleName 样式名称
     */
    public HmCheckbox(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void invalidate() {
        super.invalidate();
        this.addDrawTask(this::onDraw);
        this.setCheckedStateChangedListener(this::onCheckedChanged);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        this.setButtonElement(shapeElement);
        width = component.getWidth();
        height = component.getHeight();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setColor(new Color(Color.getIntColor(boxColor)));
        paint.setStrokeWidth(new BigDecimal(width).divide(new BigDecimal(DIVIDE_EQUALLY)).floatValue());
        canvas.drawRect(0,0, component.getWidth(), component.getHeight(), paint);
        if (this.isChecked()) {
            paint.setStyle(Paint.Style.FILL_STYLE);
            paint.setColor(new Color(Color.getIntColor(boxColor)));
            canvas.drawRect(0,0, component.getWidth(), component.getHeight(), paint);
            drawInPath(canvas);
        }
    }

    private void drawInPath(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(new BigDecimal(width).divide(new BigDecimal(DIVIDE_EQUALLY)).floatValue());
        Path path = new Path();
        path.moveTo(DIVIDE_EQUALLY, new BigDecimal(height).divide(new BigDecimal(HARF_VALUE)).floatValue());
        path.lineTo(new BigDecimal(width).divide(new BigDecimal(HARF_VALUE)).floatValue(), new BigDecimal(height)
            .subtract(new BigDecimal(DIVIDE_EQUALLY)).floatValue());
        path.lineTo(width - DIVIDE_EQUALLY, DIVIDE_EQUALLY);
        canvas.drawPath(path, paint);
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean isBoolean) {
        this.setChecked(isBoolean);
        invalidate();
        if (this.mOnCheckedChanged != null) {
            this.mOnCheckedChanged.onCheckedChanged(isBoolean);
        }
    }

    /**
     * setOnCheckedChanged方法
     * @param onCheckedChanged 是否选中
     */
    public void setOnCheckedChanged(OnCheckedChanged onCheckedChanged) {
        this.mOnCheckedChanged = onCheckedChanged;
    }

    /**
     * OnCheckedChanged 接口
     *
     * @since 2021-08-07
     */
    public interface OnCheckedChanged {
        /**
         * onCheckedChanged
         *
         * @param isBoolean
         */
        void onCheckedChanged(boolean isBoolean);
    }
}
