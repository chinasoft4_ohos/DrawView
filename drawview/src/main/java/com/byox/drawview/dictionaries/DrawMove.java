package com.byox.drawview.dictionaries;


import com.byox.drawview.enums.BackgroundScale;
import com.byox.drawview.enums.BackgroundType;
import com.byox.drawview.enums.DrawingMode;
import com.byox.drawview.enums.DrawingTool;
import com.byox.drawview.utils.SerializablePaint;
import com.byox.drawview.utils.SerializablePath;
import ohos.agp.utils.Matrix;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ing. Oscar G. Medina Cruz on 07/11/2016.
 * <p>
 * Dictionary class that save move for draw in the view, this allow the user to make a history
 * of the user movements in the view and make a redo/undo.
 *
 * @author Ing. Oscar G. Medina Cruz
 */

public class DrawMove implements Serializable {

    private static DrawMove mSingleton;

    private SerializablePaint mPaint;
    private DrawingMode mDrawingMode = null;
    private DrawingTool mDrawingTool = null;
    //private List<SerializablePath> mDrawingPathList;
    private SerializablePath mDrawingPath;
    private float mStartX, mStartY, mEndX, mEndY;
    private String mText;
    private Matrix mBackgroundMatrix;
    private PixelMap mBackgroundImage;

    // METHODS
    private DrawMove() {
    }

    /**
     * newInstance 方法
     *
     * @return mSingleton 对象
     */
    public static DrawMove newInstance() {
        mSingleton = new DrawMove();
        return mSingleton;
    }

    // GETTERS

    /**
     * getPaint 画笔方法
     *
     * @return mPaint画笔
     */
    public SerializablePaint getPaint() {
        return mPaint;
    }

    /**
     * getDrawingMode 方法
     *
     * @return mDrawingMode 模式
     */
    public DrawingMode getDrawingMode() {
        return mDrawingMode;
    }

    /**
     * getDrawingTool 工具
     *
     * @return mDrawingTool对象
     */
    public DrawingTool getDrawingTool() {
        return mDrawingTool;
    }

    /**
     * getDrawingPath路径方法
     *
     * @return mDrawingPath路径
     */
    public SerializablePath getDrawingPath() {
        return mDrawingPath;
    }

    /**
     * getStartX方法
     *
     * @return mStartX 值
     */
    public float getStartX() {
        return mStartX;
    }
    /**
     * getStartY方法
     *
     * @return mStartY值
     */
    public float getStartY() {
        return mStartY;
    }

    /**
     * getEndX方法
     *
     * @return mEndX值
     */
    public float getEndX() {
        return mEndX;
    }

    /**
     * getEndY方法
     *
     * @return mEndY值
     */
    public float getEndY() {
        return mEndY;
    }

    /**
     * getText() 方法
     *
     * @return mText值
     */
    public String getText() {
        return mText;
    }

    /**
     * getBackgroundMatrix方法
     *
     * @return mBackgroundMatrix对象
     */
    public Matrix getBackgroundMatrix(){
        return mBackgroundMatrix;
    }

    /**
     * getBackgroundImage方法
     *
     * @return mBackgroundImage 对象
     */
    public PixelMap getBackgroundImage() {
        return mBackgroundImage;
    }

    // SETTERS

    /**
     * setPaint 方法
     *
     * @param paint 画笔
     * @return mSingleton
     */
    public DrawMove setPaint(SerializablePaint paint) {
        if (mSingleton != null) {
            mSingleton.mPaint = paint;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setDrawingMode方法
     *
     * @param drawingMode 绘制模式
     * @return mSingleton 对象
     */
    public DrawMove setDrawingMode(DrawingMode drawingMode) {
        if (mSingleton != null) {
            mSingleton.mDrawingMode = drawingMode;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setDrawingTool方法
     *
     * @param drawingTool 画笔工具
     * @return mSingleton 对象
     */
    public DrawMove setDrawingTool(DrawingTool drawingTool) {
        if (mSingleton != null) {
            mSingleton.mDrawingTool = drawingTool;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setDrawingPathList设置绘制路径容器
     *
     * @param drawingPath 绘制路径
     * @return mSingleton对象
     */
    public DrawMove setDrawingPathList(SerializablePath drawingPath) {
        if (mSingleton != null) {
            mSingleton.mDrawingPath = drawingPath;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setStartX设置方法
     *
     * @param startX 值
     * @return mSingleton对象
     */
    public DrawMove setStartX(float startX) {
        if (mSingleton != null) {
            mSingleton.mStartX = startX;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setStartY方法
     *
     * @param startY 值
     * @return mSingleton对象
     */
    public DrawMove setStartY(float startY) {
        if (mSingleton != null) {
            mSingleton.mStartY = startY;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setEndX方法
     *
     * @param endX 值
     * @return mSingleton 对象
     */
    public DrawMove setEndX(float endX) {
        if (mSingleton != null) {
            mSingleton.mEndX = endX;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setEndY方法
     *
     * @param endY 值
     * @return mSingleton对象
     */
    public DrawMove setEndY(float endY) {
        if (mSingleton != null) {
            mSingleton.mEndY = endY;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setText设置文字方法
     *
     * @param text 文字
     * @return mSingleton对象
     */
    public DrawMove setText(String text) {
        if (mSingleton != null) {
            mSingleton.mText = text;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }

    /**
     * setBackgroundImage 方法
     *
     * @param backgroundImage 背景图片
     * @param backgroundMatrix 对象
     * @return mSingleton 对象
     */
    public DrawMove setBackgroundImage(PixelMap backgroundImage, Matrix backgroundMatrix) {
        if (mSingleton != null) {
            mSingleton.mBackgroundImage = backgroundImage;
            mSingleton.mBackgroundMatrix = backgroundMatrix;
            return mSingleton;
        } else throw new RuntimeException("Create new instance of DrawMove first!");
    }
}
