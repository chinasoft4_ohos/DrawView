/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byox.drawview.utils;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

/**
 * AttrValue 类
 *
 * @since 2021-08-06
 */
public class AttrValue {
    private AttrValue() {
    }

    /**
     * get方法
     *
     * @param attrSet 参数
     * @param key 参数
     * @param defValue 参数
     * @param <T> 参数
     * @return (T) defValue
     */
    @SuppressWarnings("unchecked cast")
    public static <T> T get(AttrSet attrSet, String key, T defValue) {
        if (attrSet == null || !attrSet.getAttr(key).isPresent()) {
            return (T) defValue;
        }

        Attr attr = attrSet.getAttr(key).get();
        if (defValue instanceof String) {
            return (T) attr.getStringValue();
        } else if (defValue instanceof Long) {
            return (T) (Long) (attr.getLongValue());
        } else if (defValue instanceof Float) {
            return (T) (Float) (attr.getFloatValue());
        } else if (defValue instanceof Integer) {
            return (T) (Integer) (attr.getIntegerValue());
        } else if (defValue instanceof Boolean) {
            return (T) (Boolean) (attr.getBoolValue());
        } else if (defValue instanceof Color) {
            return (T) (attr.getColorValue());
        } else if (defValue instanceof Element) {
            return (T) (attr.getElement());
        } else {
            return (T) defValue;
        }
    }

    /**
     * getLayout获取布局id
     *
     * @param attrSet 参数
     * @param key 参数
     * @param def 参数
     * @return layoutId 布局id
     */
    public static int getLayout(AttrSet attrSet, String key, int def) {
        if (attrSet.getAttr(key).isPresent()) {
            int layoutId = def;
            String value = attrSet.getAttr(key).get().getStringValue();
            if (value != null) {
                String subLayoutId = value.substring(value.indexOf(":"));
                layoutId = Integer.parseInt(subLayoutId);
            }
            return layoutId;
        }
        return def;
    }

    /**
     * getDimension 方法
     *
     * @param attrSet 属性
     * @param key 键值
     * @param defDimensionValue 定义尺寸值
     * @return attr.getDimensionValue() 尺寸值
     */
    public static int getDimension(AttrSet attrSet, String key, int defDimensionValue) {
        if (!attrSet.getAttr(key).isPresent()) {
            return defDimensionValue;
        }

        Attr attr = attrSet.getAttr(key).get();
        return attr.getDimensionValue();
    }
}
