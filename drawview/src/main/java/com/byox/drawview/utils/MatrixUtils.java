package com.byox.drawview.utils;

import ohos.agp.utils.Matrix;
import ohos.agp.utils.RectFloat;

/**
 * Created by IngMedina on 29/04/2017.
 */
public class MatrixUtils {
    /**
     * GetCenterCropMatrix构造方法
     *
     * @param srcSize 值
     * @param destSize 值
     * @return matrix对象
     */
    public static Matrix GetCenterCropMatrix(RectFloat srcSize, RectFloat destSize){
        Matrix matrix = new Matrix();
        float scale = Math.max(destSize.getWidth() / srcSize.getWidth(),
                destSize.getHeight() / srcSize.getHeight());
        matrix.setTranslate(-(destSize.getWidth() / 2), - (destSize.getHeight() / 2));
        matrix.setScale(scale, scale);

        return matrix;
    }
}
