//package com.byox.drawview.utils;
//
//import com.byox.drawview.enums.BackgroundScale;
//import com.byox.drawview.enums.BackgroundType;
//import com.byox.drawview.views.DrawView;
//import ohos.agp.components.Component;
//import ohos.agp.render.Canvas;
//import ohos.agp.render.PixelMapHolder;
//import ohos.agp.render.Texture;
//import ohos.agp.utils.Matrix;
//import ohos.media.image.PixelMap;
//import ohos.media.image.common.ScaleMode;
//import ohos.media.image.common.Size;
//import ohos.media.photokit.common.PixelMapConfigs;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//
///**
// * Created by IngMedina on 28/03/2017.
// */
//
//public class BitmapUtils {
//    public static PixelMap CreateBitmapMatchesViewSize(Component imageViewDest, PixelMap bitmapSrc) {
//        int currentBitmapWidth = bitmapSrc.getImageInfo().size.width;
//        int currentBitmapHeight = bitmapSrc.getImageInfo().size.height;
//        int ivWidth = imageViewDest.getWidth();
//        int ivHeight = imageViewDest.getHeight();
//        int newWidth = ivWidth;
//        int newHeight = (int) Math.floor((double) currentBitmapHeight * ((double) newWidth / (double) currentBitmapWidth));
//        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
//        options.size = new Size(newWidth, newHeight);
//        return PixelMap.create(bitmapSrc, options);
//    }
//
//    public static byte[] GetCompressedImage(Object image, BackgroundType backgroundType, int compressQuality) {
//
//        PixelMap bmp = null;
//        PixelMap scaledBitmap = null;
////        BitmapFactory.Options options = new BitmapFactory.Options();
//        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
//
////      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
////      you try the use the bitmap here, you will get null.
//        options.inJustDecodeBounds = true;
//        switch (backgroundType) {
//            case FILE:
//                bmp = BitmapFactory.decodeFile(((File) image).getAbsolutePath(), options);
//                break;
//            case BITMAP:
//                bmp = (PixelMap) image;
//                options.outHeight = bmp.getHeight();
//                options.outWidth = bmp.getWidth();
//                break;
//            case BYTES:
//                bmp = BitmapFactory.decodeByteArray((byte[]) image, 0, ((byte[]) image).length, options);
//                break;
//        }
//
//        int actualHeight = options.outHeight;
//        int actualWidth = options.outWidth;
//
////      max Height and width values of the compressed image is taken as 816x612
//
//        float maxHeight = 816.0f;
//        float maxWidth = 612.0f;
//        float imgRatio = actualWidth / actualHeight;
//        float maxRatio = maxWidth / maxHeight;
//
////      width and height values are set maintaining the aspect ratio of the image
//
//        if (actualHeight > maxHeight || actualWidth > maxWidth) {
//            if (imgRatio < maxRatio) {
//                imgRatio = maxHeight / actualHeight;
//                actualWidth = (int) (imgRatio * actualWidth);
//                actualHeight = (int) maxHeight;
//            } else if (imgRatio > maxRatio) {
//                imgRatio = maxWidth / actualWidth;
//                actualHeight = (int) (imgRatio * actualHeight);
//                actualWidth = (int) maxWidth;
//            } else {
//                actualHeight = (int) maxHeight;
//                actualWidth = (int) maxWidth;
//
//            }
//        }
//
////      setting inSampleSize value allows to load a scaled down version of the original image
//
//        options.inSampleSize = CalculateInSampleSize(options, actualWidth, actualHeight);
//
////      inJustDecodeBounds set to false to load the actual bitmap
//        options.inJustDecodeBounds = false;
//
////      this options allow ohos to claim the bitmap memory if it runs low on memory
//        options.inPurgeable = true;
//        options.inInputShareable = true;
//        options.inTempStorage = new byte[16 * 1024];
//
//        switch (backgroundType) {
//            case FILE:
//                bmp = BitmapFactory.decodeFile(((File) image).getAbsolutePath(), options);
//                break;
//            case BITMAP:
//                bmp = (Bitmap) image;
//                break;
//            case BYTES:
//                bmp = BitmapFactory.decodeByteArray((byte[]) image, 0, ((byte[]) image).length);
//                break;
//        }
//
//        try {
//            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//        }
//
//        float ratioX = actualWidth / (float) options.outWidth;
//        float ratioY = actualHeight / (float) options.outHeight;
//        float middleX = actualWidth / 2.0f;
//        float middleY = actualHeight / 2.0f;
//
//        Matrix scaleMatrix = new Matrix();
//        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
//
//        Canvas canvas = new Canvas(scaledBitmap);
//        canvas.setMatrix(scaleMatrix);
//        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
//
////      check the rotation of the image and display it properly
//        ExifInterface exif;
//        try {
//            Matrix matrix = new Matrix();
//            if (backgroundType == BackgroundType.FILE) {
//                exif = new ExifInterface(((File) image).getAbsolutePath());
//
//                int orientation = exif.getAttributeInt(
//                        ExifInterface.TAG_ORIENTATION, 0);
//                Log.d("EXIF", "Exif: " + orientation);
//                if (orientation == 6) {
//                    matrix.postRotate(90);
//                    Log.d("EXIF", "Exif: " + orientation);
//                } else if (orientation == 3) {
//                    matrix.postRotate(180);
//                    Log.d("EXIF", "Exif: " + orientation);
//                } else if (orientation == 8) {
//                    matrix.postRotate(270);
//                    Log.d("EXIF", "Exif: " + orientation);
//                }
//            }
//
//            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
//                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
//                    true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
////        try {
////            returnFile = File.createTempFile("temp", "." + (
////                    image.getAbsolutePath().toLowerCase().endsWith("jpg") ? "jpg" : "png"));
////            FileOutputStream out = null;
////            String filename = returnFile.getAbsolutePath();
//        ByteArrayOutputStream stream = null;
////            try {
////                out = new FileOutputStream(filename);
//        stream = new ByteArrayOutputStream();
//
////          write the compressed bitmap at the destination specified by filename.
//        scaledBitmap.compress(Bitmap.CompressFormat.PNG, compressQuality, stream);
//        bmp.recycle();
//
////            } catch (FileNotFoundException e) {
////                e.printStackTrace();
////            }
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//
//        return stream.toByteArray();
//
//    }
//
//    public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
//        final int height = options.outHeight;
//        final int width = options.outWidth;
//        int inSampleSize = 1;
//
//        if (height > reqHeight || width > reqWidth) {
//            final int heightRatio = Math.round((float) height / (float) reqHeight);
//            final int widthRatio = Math.round((float) width / (float) reqWidth);
//            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
//        }
//        final float totalPixels = width * height;
//        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
//        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
//            inSampleSize++;
//        }
//
//        return inSampleSize;
//    }
//
//    public static PixelMap GetBitmapForDrawView(DrawView drawView, Object image, BackgroundType backgroundType, int imageQuality) {
//        byte[] imageInBytes = BitmapUtils.GetCompressedImage(image, backgroundType, imageQuality);
//        return BitmapUtils.CreateBitmapMatchesViewSize(drawView,
//                BitmapFactory.decodeByteArray(imageInBytes, 0, imageInBytes.length));
//    }
//
//    public static PixelMap GetCombinedBitmaps(PixelMap bmp1, PixelMap bmp2, int destWidth, int destHeight) {
//
//        PixelMap bmOverlay = Bitmap.createBitmap(destWidth, destHeight, bmp1.getConfig());
//        Texture texture = new Texture(bmOverlay);
//        Canvas canvas = new Canvas(texture);
////        canvas.drawBitmap(bmp1, new Matrix(), null);
////        canvas.drawBitmap(bmp2, 0, 0, null);
//        canvas.drawPixelMapHolder(new PixelMapHolder(bmp1), 0, 0, null);
//        canvas.drawPixelMapHolder(new PixelMapHolder(bmp2), 0, 0, null);
//        return bmOverlay;
//    }
//}
