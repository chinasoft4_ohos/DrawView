package com.byox.drawview.views;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;

/**
 * AttrValue 类
 *
 * @since Created by IngMedina on 30/03/2017.
 */
public class ZoomRegionView extends Image {

    // LISTENER
    private OnZoomRegionListener mOnZoomRegionListener;

    // VARS
    private PixelMap mParentContent;
    private Paint mZoomedRegionPaint;

    private RectFloat mZoomedRegion;
    private RectFloat mSourceRect;
    private RectFloat mDestRect;
    private Paint mClearPaint;
    private Paint bgPaint;
    private int mCurrentMotionEvent = -1;
    private boolean mMoveZoomArea = false;
    private float mStartTouchX, mStartTouchY;

    /**
     * ZoomRegionView构造方法
     *
     * @param context 上下文
     */
    public ZoomRegionView(Context context) {
        super(context);
        initZoomRegion();
    }

    /**
     * ZoomRegionView构造方法
     *
     * @param context 上下文
     * @param attrs 属性参数
     */
    public ZoomRegionView(Context context, AttrSet attrs) {
        super(context, attrs);
        initZoomRegion();
    }

    /**
     * ZoomRegionView构造方法
     *
     * @param context 上下文
     * @param attrs 属性参数
     * @param defStyleAttr 样式
     */
    public ZoomRegionView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initZoomRegion();
    }

    /**
     * onDraw方法
     *
     * @param component 组件
     * @param canvas 动画
     */
    protected void onDraw(Component component, Canvas canvas) {

        canvas.drawRect(mDestRect, bgPaint);

        if (mZoomedRegion != null) {
            canvas.drawRect(mDestRect, mZoomedRegionPaint);
            canvas.drawPixelMapHolderRect(new PixelMapHolder(mParentContent), mSourceRect, mDestRect, bgPaint);
//            mZoomOverlay.eraseColor(Color.TRANSPARENT);
//            mCanvasOverlay.drawRect(mDestRect, mZoomedRegionPaint);
            canvas.drawRect(mZoomedRegion, mClearPaint);
//            canvas.drawBitmap(mZoomOverlay, 0, 0, null);
            //canvas.drawPixelMapHolder(new PixelMapHolder(mZoomOverlay), 0, 0, bgPaint);
        }
//        super.onDraw(canvas);
    }

    /**
     * onTouchEvent方法
     *
     * @param v 组件
     * @param event 按键事件
     * @return boolean类型
     */
    public boolean onTouchEvent(Component v, TouchEvent event) {
        float touchX = event.getPointerPosition(0).getX();
        float touchY = event.getPointerPosition(0).getY();

        switch (event.getAction()){
            case TouchEvent.PRIMARY_POINT_DOWN:
                mCurrentMotionEvent = TouchEvent.PRIMARY_POINT_DOWN;
                mMoveZoomArea = false;
                if (touchX >= mZoomedRegion.left && touchX <= mZoomedRegion.right
                        && touchY >= mZoomedRegion.top && touchY <= mZoomedRegion.bottom) {
                    mMoveZoomArea = true;
                    mStartTouchX = touchX;
                    mStartTouchY = touchY;
                }
                break;
            case TouchEvent.POINT_MOVE:
                if ((mCurrentMotionEvent == TouchEvent.PRIMARY_POINT_DOWN
                        || mCurrentMotionEvent == TouchEvent.POINT_MOVE) && mMoveZoomArea){
                    mCurrentMotionEvent = TouchEvent.POINT_MOVE;
                    RectFloat preview = new RectFloat(
                            mZoomedRegion.left + (int) (touchX - mStartTouchX),
                            mZoomedRegion.top + (int) (touchY - mStartTouchY),
                            mZoomedRegion.right + (int) ((touchX - mStartTouchX)),
                            mZoomedRegion.bottom + (int) ((touchY - mStartTouchY)));
                    if (preview.left >= 0 && preview.right <= getWidth()
                            && preview.top >= 0 && preview.bottom <= getHeight()) {
                        mZoomedRegion = preview;
                        invalidate();
                    }
                    mStartTouchX = touchX;
                    mStartTouchY = touchY;
                    if (mOnZoomRegionListener != null)
                        mOnZoomRegionListener.onZoomRegionMoved(mZoomedRegion);
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                mCurrentMotionEvent = TouchEvent.PRIMARY_POINT_UP;
                mMoveZoomArea = false;
                break;
        }
        return true;
    }

    // METHODS
    private void initZoomRegion() {
//        mZoomedRegionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mZoomedRegionPaint = new Paint();
        mZoomedRegionPaint.setAntiAlias(true);
        mZoomedRegionPaint.setColor(Color.GRAY);
//        mZoomedRegionPaint.setAlpha(50/255.0F);
        mClearPaint = new Paint();
        mClearPaint.setColor(Color.WHITE);
        mClearPaint.setAlpha(0.7F);
//        mClearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
//        mClearPaint.setBlendMode(BlendMode.SRC_OUT);
//        setOnTouchListener(this);
        bgPaint = new Paint();
        bgPaint.setColor(Color.GRAY);
        this.addDrawTask(this::onDraw);
        this.setTouchEventListener(this::onTouchEvent);
    }

    /**
     * updateParent更新方法
     *
     * @param parentContent 参数
     */
    public void updateParent(PixelMap parentContent) {
        this.mParentContent = parentContent;
        invalidate();
    }

    /**
     * drawZoomRegion方法
     *
     * @param parentContent 父类组件
     * @param sourceRect 参数
     * @param scaleFactor 参数
     */
    public void drawZoomRegion(PixelMap parentContent, Rect sourceRect, float scaleFactor) {

        System.out.println("######## drawZoomRegion");
        this.mParentContent = parentContent;
        mZoomedRegion = new RectFloat((int) (sourceRect.left / scaleFactor), (int) (sourceRect.top / scaleFactor),
                (int) (sourceRect.right / scaleFactor), (int) (sourceRect.bottom / scaleFactor));
        mSourceRect = new RectFloat(0, 0, mParentContent.getImageInfo().size.width,
                mParentContent.getImageInfo().size.height);
        mDestRect = new RectFloat(0, 0, getWidth(), getHeight());
//        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
//        options.size = new Size(getWidth(), getHeight());
//        options.pixelFormat = PixelFormat.ARGB_8888;
//        mZoomOverlay = PixelMap.create(options);
//        Bitmap init = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
//        mZoomOverlay = init.copy(Bitmap.Config.ARGB_8888, true);
//        init.recycle();
//        Texture texture = new Texture(mZoomOverlay);
//        mCanvasOverlay = new Canvas(texture);
        invalidate();
    }

    // GETTERS & SETTERS
    private void setZoomedRegionStrokeWidth(float strokeWidth) {
        this.mZoomedRegionPaint.setStrokeWidth(strokeWidth);
    }

    private void setZoomedRegionStrokeColor(int strokeColor){
        this.mZoomedRegionPaint.setColor(new Color(strokeColor));
    }

    /**
     * getZoomedRegionStrokeWidth方法
     *
     * @return this.mZoomedRegionPaint.getStrokeWidth() 宽度
     */
    public float getZoomedRegionStrokeWidth() {
        return this.mZoomedRegionPaint.getStrokeWidth();
    }

    /**
     * getZoomedRegionStrokeColor方法
     *
     * @return this.mZoomedRegionPaint.getColor().getValue()
     */
    public int getZoomedRegionStrokeColor(){
        return this.mZoomedRegionPaint.getColor().getValue();
    }

    /**
     * setOnZoomRegionListener 方法
     *
     * @param onZoomRegionListener 监听
     */
    public void setOnZoomRegionListener(OnZoomRegionListener onZoomRegionListener){
        mOnZoomRegionListener = onZoomRegionListener;
    }

    /**
     * OnZoomRegionListener 接口
     *
     * @since 2021-08-06
     */
    public interface OnZoomRegionListener{
        /**
         * onZoomRegionMoved方法
         *
         * @param newRect 对象
         */
        void onZoomRegionMoved(RectFloat newRect);
    }
}
